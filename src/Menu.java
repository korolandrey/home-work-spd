import java.util.Scanner;

public class Menu {

    private static String[] lections = new String[0];
    private static String[][] resurses = new String[0][0];

    public static void mainMenu(){

        while (true) {
            printMainMenu();
            System.out.print("Введите пункт меню : ");
            Scanner scan = new Scanner(System.in);
            int menuItem = scan.nextInt();
            switch (menuItem) {
                case 1:
                    printLectionList(lections);
                    break;
                case 2:
                    lections = addLection(lections);
                    break;
                case 3:
                    lections = deleteLection(lections);
                    break;
                case 4:
                    lectionMenu();
                    break;
                case 5:
                    System.exit(1);
                    break;
            }
        }
    }

   public static void lectionMenu(){
        while (true) {
            printLectionMenu();
            System.out.print("Введите пункт меню : ");
            Scanner scan = new Scanner(System.in);
            int menuItem = scan.nextInt();
            switch (menuItem) {
                case 1:
                    System.out.println("1");
                    break;
                case 2:
                    System.out.println("2");
                    break;
                case 3:
                    System.out.println("3");
                    break;
                case 4:
                    mainMenu();
                    break;
            }
        }
    }


    public static void printMainMenu() {
        System.out.println("1. Вывести список лекций");
        System.out.println("2. Добавить новую лекцию");
        System.out.println("3. Удалить лекцию");
        System.out.println("4. Выбрать лекцию");
        System.out.println("5. Выход");
    }

    public static void printLectionMenu() {
        System.out.println("1. Список литературы");
        System.out.println("2. Добавить литературу");
        System.out.println("3. Удалить литературу");
        System.out.println("4. Выход в основное меню");
    }

    public static void printLectionList(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(i + "." + array[i]);
        }

    }

    public static String[] addLection(String[] array) {
        String [] temp = array.clone();
        array = new String[array.length + 1];
        System.arraycopy(temp, 0, array, 0, temp.length);

        System.out.print("Введите название лекции : ");
        Scanner scan = new Scanner(System.in);
        String newLection = scan.nextLine();
        array [array.length - 1] = newLection;
        return array;
    }

    public static String[] deleteLection(String [] array) {
        System.out.print("Введите номер лекции которую хотите удалить : ");
        Scanner scan = new Scanner(System.in);
        int numLection = scan.nextInt();
        array[numLection] = null;
        return array;
    }


}